<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\StudentAccount;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function show(){
        $data = StudentAccount::with(['student', 'student.attendance'])->find(Auth::id());
        return response()->json($data, 200);
    }

    public function index(Request $request){
        $student = Student::with(['gender', 'section'])
            ->where('adviser_id', Auth::id())
            ->where('semester_id', $request->semester_id)
            ->where('school_year_id', $request->school_year_id)
            ->where('section_id', $request->section_id)->get();
        return response()->json($student);
    }

    public function store(Request $request){

        $data = [
            'id' => $request->id,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'section_id' => $request->section_id,
            'semester_id' => $request->semester_id,
            'school_year_id' => $request->school_year_id,
            'gender_id' => $request->gender_id,
            'adviser_id' => Auth::id()
        ];

        Student::create($data);

        $account = [
            'student_id' => $request->id,
            'password' => Hash::make($request->id.$request->last_name)
        ];

        StudentAccount::create($account);

        return response()->json(['msg' => 'Student added successfully!'], 200);
    }

    public function destroy($id){
        Student::destroy($id);
        return response()->json(['msg' => 'Student record deleted successfully!'], 200);
    }

    public function update(Request $request, $id){
        try {
            $student = Student::where('id', $id)->firstOrFail();
            $student->update([
                'id' => $request->id,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'section_id' => $request->section_id,
                'gender_id' => $request->gender_id,
                'instructor_id' => Auth::id(),
            ]);

            $updated = Student::where('id', $id)->firstOrFail();
            return response()->json($updated);

        } catch(ModelNotFoundException $exception) {
            return response()->json(['message' => 'Student not found']);
        }
    }

    public function student_attendance(Request $request){
        $data = Student::with(['section', 'gender', 'attendance'])
            ->where('adviser_id', Auth::id())
            ->where('semester_id', $request->semester_id)
            ->where('school_year_id', $request->school_year_id)
            ->where('section_id', $request->section_id)->get();

        return response()->json($data, 200);
    }

    public function attendance(){
        $data = Student::with(['section', 'gender', 'attendance'])->where('id', Auth::id())->first();
        return response()->json($data, 200);
    }

}
