<?php

use App\Http\Controllers\AdviserController;
use App\Http\Controllers\GenderController;
use App\Http\Controllers\SchoolYearController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\SemesterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\student_controller;
use App\Http\Controllers\StudentAccountController;
use App\Http\Controllers\StudentAttendanceController;
use App\Http\Controllers\StudentController;
use App\Models\Attendance;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'adviser', 'middleware'=>'api'], function () {
    Route::post('store', [AdviserController::class, 'store']);
    Route::post('me', [AdviserController::class, 'me']);
    Route::post('login', [AdviserController::class, 'login']);
    Route::post('logout', [AdviserController::class, 'logout']);
    Route::put('update', [AdviserController::class, 'update']);
    Route::post('uploadImage', [AdviserController::class, 'uploadImage']);
    Route::apiResource('section', SectionController::class);
});

Route::apiResource('student', StudentController::class);
Route::get('student/attendance', [StudentController::class, 'attendance']);
Route::post('/studentattendance', [StudentController::class, 'student_attendance']);

Route::group(['prefix' => 'student'], function () {
    Route::post('me', [StudentAccountController::class, 'me']);
    Route::post('login', [StudentAccountController::class, 'login']);
    Route::post('logout', [StudentAccountController::class, 'logout']);
});

Route::get('/semester', [SemesterController::class, 'index']);
Route::get('/schoolyear', [SchoolYearController::class, 'index']);

Route::get('section', [SectionController::class, 'index']);
Route::get('gender', [GenderController::class, 'index']);

/*ATTENDANCE*/
Route::post('/timein', [StudentAttendanceController::class, 'timein']);
Route::post('/timeout', [StudentAttendanceController::class, 'timeout']);
Route::get('/attendance', [StudentAttendanceController::class, 'index']);




