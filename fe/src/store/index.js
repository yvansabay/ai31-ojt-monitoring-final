import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex);

import auth from "./modules/authentication";
import user from "./modules/user";
import sem from "./modules/semester";
import student from "./modules/student";
import attendance from "./modules/attendance";
import student_attendance from "./modules/student_user/attendance";


export default new Vuex.Store({
  modules: {
    auth, user, student, sem, attendance, student_attendance
  }
})