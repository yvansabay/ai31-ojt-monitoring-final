import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../../config/config'

Vue.use(Vuex);

const student = 'student'

export default ({
  namespaced: true,
  state: {
    attendance: [],
  },
  actions: {
    async getData({commit}){
      const res = await AXIOS.get(`${student}/attendance?token=` + localStorage.getItem('auth')).then(response => {
        commit('SET_ATTENDANCE', response.data)
        return response
      }).catch(error => {
        return error.response
      })

      return res
    },
  },
  getters: {
    getAttendance(state){
      return state.attendance
    }
  },
  mutations: {
    SET_ATTENDANCE(state, data){
      state.attendance = data
    }
  },
})