import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../config/config'

Vue.use(Vuex);

const student = 'student'

export default ({
  namespaced: true,
  state: {
    attendance: [],
    student: [],
  },
  actions: {
   
  },
  getters: {

  },
  mutations: {
    
  },
})