import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../config/config'

Vue.use(Vuex);

export default ({
  namespaced: true,
  state: {
    attendance: [],
    student_attendance: [],
  },
  actions: {
   
    //ATTENDANCE
    async timeIn({commit}, data){
      const res = await AXIOS.post('timein', data).then(response => {
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    },

    async timeOut({commit}, data){
      const res = await AXIOS.post('timeout', data).then(response => {
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    },

    async getAttendanceRecord({commit}, data){
      const res = await AXIOS.get('attendance').then(response => {
        commit('SET_ATTENDANCES', response.data)
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    },

    async getStudentAttendanceRecord({commit},data){
      const res = await AXIOS.post(`studentattendance?section_id=${data.section_id}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        commit('SET_STUDENT_ATTENDANCE', response.data)
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    }
  },
  getters: {
    getAttendance(state){
      return state.attendance
    },
    getStudentAttendance(state){
      return state.student_attendance
    }
  },
  mutations: {
   SET_ATTENDANCES(state, data){
    state.attendance = data
   },
   SET_STUDENT_ATTENDANCE(state, data){
     state.student_attendance = data
   }
  },
})