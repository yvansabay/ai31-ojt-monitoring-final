import Vue from "vue";
import Vuex from "vuex";
import AXIOS from "../../config/config";

Vue.use(Vuex);

const adviser = "adviser";
const student = "student";

export default {
  namespaced: true,
  state: {
    sections: [],
    gender: [],
    students: [],
  },
  actions: {
    async storeUser({ commit }, data) {
      const res = await AXIOS.post(`${adviser}/store`, data)
        .then((response) => {
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async updateUser({ commit }, data) {
      const res = await AXIOS.put(`${adviser}/update`, data)
        .then((response) => {
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async getGender({ commit }) {
      const res = await AXIOS.get("gender")
        .then((response) => {
          commit('SET_GENDER',response.data)
          return response;
        })
        .catch((error) => {
          return error.response;
        });

      return res;
    },
    async fetchSections({ commit }) {
      await AXIOS.get("section")
        .then((response) => {
          commit("SET_SECTIONS", response.data);
        })
        .catch((error) => {
          console.log(error.response);
        });
    },

    // Adviser - Student

    async fetchStudents({ commit }) {
      const response = await AXIOS.get('adviser/student')
        .then((res) => {
          commit("SET_STUDENTS", res.data);
          return res.data;
        })
        .catch((err) => {
          err.response;
        });
      return response;
    },

    async saveStudent({ commit }, data) {
      const response = await AXIOS.post('adviser/student', data)
        .then((res) => {
          return res.data;
        })
        .catch((err) => {
          err.response;
        });
      return response;
    },

    async updateStudent({ commit }, { data, id }) {
      const response = await AXIOS.put(`adviser/student/${id}`, data)
        .then((res) => {
          return res.data;
        })
        .catch((err) => {
          err.response;
        });
      return response;
    },

    async deleteStudent({ commit }, id) {
      const response = await AXIOS.delete(`adviser/student/${id}`)
        .then((res) => {
          return res.data;
        })
        .catch((err) => {
          err.response;
        });
      return response;
    },
  },
  getters: {
    getStudents: (state) => state.students,
    getSections: (state) => state.sections,
    getGender: (state) => state.gender
  },
  mutations: {
    SET_SECTIONS(state, data) {
      state.sections = data;
    },
    SET_STUDENTS(state, data) {
      state.students = data;
    },
    SET_GENDER(state, data) {
      state.gender = data;
    },
    // Student
  },
};
