import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../config/config'

Vue.use(Vuex);

const section = 'section'
const student = 'student'

export default ({
  namespaced: true,
  state: {
    section: [],
    student: [],
  },
  actions: {
    //STUDENT SECTION
    async saveStudent({commit}, data){
      const res = await AXIOS.post(`${student}`, data).then(response => {
        return response
      }).catch(error => {
        return error.response
      });

      return res
    },
    async updateStud({commit}, {id, data}){
      const res = await AXIOS.put(`${student}/${id}`, data).then(response => {
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    },
    async deleteStudent({commit}, id){
      const res = await AXIOS.delete(`${student}/${id}`).then(response => {
        commit('DELETE_STUDENT', id)
        return response
      }).catch(error => {
        return error.response
      });

      return res
    },
    async getStudents({commit}, data){
      const res = await AXIOS.get(`${student}?section_id=${data.section_id}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        commit('SET_STUDENT', response.data)
        return response
      }).catch(error => {
        return error.response
      })

      return res
    },

    //SECTION
    async saveSection({commit}, data){
      const res = await AXIOS.post(`adviser/${section}`, data).then(response => {
        commit('ADD_SECTION', response.data.section)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async getSections({commit}, data){
      const res = await AXIOS.get(`adviser/${section}?semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response =>{
        commit('SET_SECTION', response.data)
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    },
    async deleteSec({commit}, id){
      const res = await AXIOS.delete(`adviser/${section}/${id}`).then(response => {
        commit('DELETE_SECTION', id);
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    },
    async updateSec({commit}, {index, data}){
      const res = await AXIOS.put(`adviser/${section}/${data.id}`, data).then(response => {
        commit('UPDATE_SECTION', {id: index, data: response.data})
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    },

    //ATTENDANCE
    async timeIn({commit}, data){
      const res = await AXIOS.post('timein', data).then(response => {
        return response
      }).catch(error => {
        return error.response
      })

      return res;
    }
  },
  getters: {
   getSection(state){
     return state.section
   },
   getStudent(state){
     return state.student
   }
  },
  mutations: {
    SET_STUDENT(state, data){
      state.student = data
    },
    DELETE_STUDENT(state, id){
      state.student = state.student.filter(student => {
        return student.id !== id;
      });
    },

    //SECTION
    ADD_SECTION(state, data){
      state.section.push(data)
    },
    SET_SECTION(state, data){
      state.section = data
    },
    DELETE_SECTION(state, id){
      state.section = state.section.filter(section => {
        return section.id !== id;
      });
    },
    UPDATE_SECTION(state, {id, data}){
      Vue.set(state.section, id, data);
    }

  },
})